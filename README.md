# Group20_TeamResearch

Group number - 20

Team members

- Rahul Puthukkot
- Arun Joseph
- Kiran Maria Abraham
- Saloni Gude
- Harshadeep Nimmagadda

This data set contains information about used cars:
	Name: The brand and model of the car.
	Location: The location in which the car is being sold or is available for purchase.
	Year: The year or edition of the model.
	Kilometers_Driven: The total kilometres driven in the car by the previous owner(s) in KM.
	Fuel_Type: The type of fuel used by the car. (Petrol, Diesel, Electric, CNG, LPG)
	Transmission: The type of transmission used by the car. (Automatic / Manual)
	Owner_Type:
	Mileage: The standard mileage offered by the car company in kmpl or km/kg
	Engine: The displacement volume of the engine in CC.
	Power: The maximum power of the engine in bhp.
	Seats: The number of seats in the car.
	New_Price: The price of a new car of the same model.
	

Kaggle URL: https://www.kaggle.com/avikasliwal/used-cars-price-prediction

Original file 'train-data.csv' is modified as follows:	
	Removed the rows corresponding to missing values in the col 'New_Price'. Rows reduced to 824	
    Removed below row:	
			Mahindra E Verito D4 Chennai	2016	50000	Electric	Automatic	First		72 CC	41 bhp	5	13.58 Lakh	13
			The above row is the only row for electric vehicle. Hence dropped.	
	Removed 'kmpl' and 'km/kg' from col 'Mileage'	
	Removed 'CC' from col 'Engine'	
	Removed 'bhp' from col 'Power'	
	Converted 'New_Price' and 'Price' to Millions (Rupees)	

Git workflow
- Development branch used - `develop`

- checkout a new branch from the `develop` branch for each individual task using
`git checkout -b <branch-name>`

- add the required files
`git add <filenames>`

- add proper commit messages for the task done
`git commit -m "<commit-message>"`

- push the changes to the remote origin
`git push origin <branch-name>`

- Create Merge request to develop `branch` and assign a team member to review

- Merge the feature branch to the `develop` branch

Keep Master branch clean (never push directly to master branch)