﻿* Encoding: UTF-8.
VALUE LABELS Fuel_Type 1 'Petrol' 2 'Diesel' 3 'CNG' 4 'LPG' 5 'Electric'.
VALUE LABELS EngineCat .00 'Large' 1.00 'Small'.
VALUE LABELS Transmission 1 'Manual' 2 'Automatic'.
VALUE LABELS Location 1 'Ahmedabad' 2 'Bangalore' 3 'Chennai' 4 'Coimbatore' 5 'Delhi' 6 'Hyderabad' 7 'Jaipur' 8 'Kochi' 9 'Kolkata' 10 'Mumbai' 11 'Pune'.
VALUE LABELS Manufacturer 1 'Honda' 2 'Toyota' 3 'Maruti' 4 'Mitsubishi' 5 'BMW' 6 'Volkswagen' 7 'Hyundai' 8 'Mercedes-Benz' 9 'Nissan' 10 'Tata' 11 'Datsun' 12 'Audi' 13 'Mahindra' 14 'Skoda' 15 'Mini' 16 'Jaguar' 17 'Jeep' 18 'Ford' 19 'Renault' 20 'Land Rover' 21 'Volvo' 22 'Fiat' 23 'Isuzu' 24 'Porsche'.

 

